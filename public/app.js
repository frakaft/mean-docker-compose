var app = angular.module('soa', ['ngRoute', 'mwl.confirm', 'ui.bootstrap', 'ngSanitize']);

app.constant('URL_BASE', '/');
app.constant('URL_API_BASE', '/api/v1/');

app.run(['$location', '$log', '$rootScope',
    function ($location, $log, $rootScope) {
        $log.log('Iniciando app');

        $rootScope.relocate = function (loc) {
            $location.path(loc);
        };
    }
]);
