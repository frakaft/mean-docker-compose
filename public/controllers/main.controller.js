angular.module('soa').controller('MainController',
    function ($scope, $log, coreService) {

        $scope.title = 'Tasks';
        $scope.tasks = [];
        $scope.instance = {};

        $scope.add = function () {
            coreService.add($scope.instance).then(
                function (resp) {
                    $scope.tasks.push(resp.data);
                    $scope.instance = {};
                },
                function (err) {
                    $log.log(err);
                }
            );
        };

        $scope.remove = function (id) {
            coreService.remove(id).then(
                function (resp) {
                    for (var index in $scope.tasks) {
                        if ($scope.tasks[index]._id === id) {
                            $scope.tasks.splice(index, 1);
                            break;
                        }
                    }

                },
                function (err) {
                    $log.log(err);
                }
            );
        };

        $scope.updateDone = function (task) {
            task.done = !task.done;
            coreService.update(task._id, task).then(
                function (resp) {
                    for (var index in $scope.tasks) {
                        if ($scope.tasks[index]._id === task._id) {
                            $scope.tasks[index].done = task.done;
                            break;
                        }
                    }

                },
                function (err) {
                    $log.log(err);
                }
            );
        };

        $scope.getAll = function () {
            coreService.getAll().then(
                function (resp) {
                    $scope.tasks = resp.data;
                },
                function (err) {
                    $log.log(err);
                }
            );
            $log.log($scope.tasks)
        };

        $scope.getOne = function () {
            coreService.getOne(id).then(
                function (resp) {
                    $scope.instance = resp.data;
                },
                function (err) {
                    $log.log(err);
                }
            );
        };

        $scope.showButton = function () {
            if(!$scope.instance.name){
                return true;
            }
            return false;
        };

        $scope.showEmpty = function () {
            if($scope.tasks.length === 0){
                return true;
            }
            return false;
        };

        $scope.formatDate = function (date) {
            var fdate = new Date(date);
            var dd = ("0" + fdate.getDate()).slice(-2);
            var mm = ("0" + fdate.getMonth() + 1).slice(-2);
            var yyyy = fdate.getFullYear()
            var HH = ("0" + fdate.getHours()).slice(-2);
            var MM = ("0" + fdate.getMinutes()).slice(-2);
            var SS = ("0" + fdate.getSeconds()).slice(-2);
            return dd + '/' + mm + '/' + yyyy + ' ' + HH + ':' + MM + ':' + SS + "hs";
        };

        $scope.getAll();
        $log.log($scope.tasks)
    }
);