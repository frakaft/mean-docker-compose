angular.module('soa')
    .factory('coreService', function ($http, URL_API_BASE) {
        return {
            getAll: function () {
                return $http.get(URL_API_BASE + "tasks");
            },
            getOne: function (id) {
                return $http.get(URL_API_BASE + "tasks/" + id);
            },
            add : function(task){
                return $http.post(URL_API_BASE+"tasks", task);
            },
            remove : function(id){
                return $http.delete(URL_API_BASE+"tasks/" + id);
            },
            update : function(id, task){
                return $http.put(URL_API_BASE+"tasks/" + id, task);
            }
        }
    });