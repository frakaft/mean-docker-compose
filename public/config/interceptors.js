angular.module('soa')
    .service('APIInterceptor', function ($rootScope, $log) {
        var service = this;

        service.responseError = function (response) {
            if (response.status === 500) {
                $rootScope.toCidi();
            } else {
                swal({
                    title: response.status,
                    text: response.statusText,
                    type: "error",
                    allowEscapeKey: true,
                    showConfirmButton: true
                });
                $log.log('ERROR');
                $log.log(response);
            }
            return response;
        };
    });