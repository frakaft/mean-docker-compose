angular.module('soa')
    .config(function ($routeProvider, $locationProvider, $httpProvider, $logProvider) {
        console.log('Configurando...');

        $logProvider.debugEnabled(true);

        $httpProvider.defaults.withCredentials = true;
        $httpProvider.interceptors.push('APIInterceptor');

        $locationProvider.hashPrefix('');
        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainController'
            })
            .otherwise({
                redirectTo: '/'
            });
    });