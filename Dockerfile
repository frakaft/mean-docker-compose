FROM ubuntu:bionic

RUN apt-get -y update
RUN apt-get -y install nodejs
RUN apt-get -y install npm
RUN apt-get -y install netcat

WORKDIR /usr/src/app

COPY . .

RUN npm install

CMD bash wait_for_mongo.sh
CMD node bin/www