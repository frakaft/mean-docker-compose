var mongoose = require('mongoose')

var Task = new mongoose.Schema({
    name: {type:String, trim:true, default:''},
    date: {type: Date, default: Date.now},
    done: {type: Boolean, default: false}
});

module.exports = mongoose.model('Task', Task);