var express = require('express');
var path = require('path');
var Task = require(path.resolve('model/task'));
var router = express.Router();

router.get('/*', function (req, res, next) {
    var id = req.params[0];
    if (id) {
        Task.findOne({_id: id}).exec(function (err, task) {
            if (err) {
                console.log("Error:", err);
                res.status(404).send('not found');
            }
            else {
                console.log("Task "+ id +":");
                console.log(task);
                res.status(200).send(task);
            }
        });
    } else {
        Task.find({}).exec(function (err, tasks) {
            if (err) {
                console.log("Error:", err);
                res.status(500).send('error no manejado');
            }
            else {
                //console.log("All Tasks:");
                //console.log(tasks);
                res.status(200).send(tasks);
            }
        });
    }
});

router.post('/*', function (req, res, next) {
    var task = new Task(req.body);

    task.save(function(err) {
        if(err) {
            console.log(err);
            res.status(500).send('error no manejado');
        } else {
            console.log("New task "+ task._id +" saved!");
            res.status(201).send(task);
        }
    });
});

router.delete('/*', function (req, res, next) {
    var id = req.params[0];
    Task.remove({_id: id}, function(err, task) {
        if(err) {
            console.log(err);
            res.status(404).send('not found');
        }
        else {
            console.log("Task "+ id +" deleted!");
            res.status(200).send(task);
        }
    });
});

router.put('/*', function (req, res, next) {
    var id = req.params[0];
    var task = new Task(req.body);
    Task.update({_id: id},{name:task.name, done:task.done}, {new: true}, function(err, task) {
        if(err) {
            console.log(err);
            res.status(404).send('not found');
        }
        else {
            console.log("Task "+ id +" updated!");
            res.status(200).send(task);
        }
    });
});

module.exports = router;