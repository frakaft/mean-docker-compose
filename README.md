# mean-docker-compose
> Simple MEAN project with docker-compose and nginx.

[Install docker](#Docker-CE-for-Ubuntu-x86_64-/-amd64)
 
[Install docker-compose](#Install-docker-compose)

[Build and run the project](#Docker-Compose)

## Docker and Docker Compose
## Docker CE for Ubuntu x86_64 / amd64
### Uninstall old versions
```
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```

### Install from repo
```
$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

$ sudo apt-get update

$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### Install from package

##### Dounload yout package from: https://download.docker.com/linux/ubuntu/dists/
```
$ sudo dpkg -i /path/to/package.deb
```

### First run
```
$ sudo docker run hello-world
```

## Post instalation: Manage Docker as a non-root user
```
$ sudo groupadd docker

$ sudo usermod -aG docker $USER
```

> Then Log out and log back in so that your group membership is re-evaluated

### Test setup
```
$ docker run hello-world
```

## Install docker compose
```
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

$ sudo chmod +x /usr/local/bin/docker-compose
```

## Install command completion (Optional)
```
$ sudo curl -L https://raw.githubusercontent.com/docker/compose/1.24.0/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
```

## Run any Docker HUB container
```
$ docker run -it --rm -d -p <LOCAL PORT>:<CONTAINER PORT> -v <LOCAL FS>:<CONTAINER FS> --name <NAME> <USER>:<REPO>
```

### Important options
```
-t                    			: Allocate a pseudo-tty
-i                    			: Keep STDIN open even if not attached
-d                    			: Detach mode demond
-p <HOST PORT>:<CONTAINER PORT> : Bind host and container ports
-v <HOST FS>:<CONTAINER FS> 	: Shared filesystems
--rm                   			: Remove on exit
--expose <PORT>		  			: Port to expose on container
--name <NAME>         			: Local container name
--network <NETWORK>   			: Network
```

### Example:
```
$ docker run -it --name my_ubuntu_bionic ubuntu:bionic
```

## Usefull commands

##### List running containers
```
$ docker ps
```

##### List all containers
```
$ docker ps -a
```

##### List container info
```
$ docker inspect <CONTAINER ID/NAME>
```

##### Start container
```
$ docker container start <CONTAINER ID/NAME>
```

##### Stop container
```
$ docker container stop <CONTAINER ID/NAME>
```

##### Remove container
```
$ docker container rm <CONTAINER ID/NAME>
```

##### Remove multiple containers
```
$ docker rm <MULTIPLE CONTAINER's ID/NAME>
```

##### Excecute a command on container
```
-t : Allocate a pseudo-tty
-i : Keep STDIN open even if not attached
```

```
$ docker exec -it <CONTAINER ID/NAME> <COMMAND>
```

##### SSH into container
```
$ docker exec -it <CONTAINER ID/NAME> /bin/bash
```

##### Remove all stopped containers, unused networks, dangling images and build cache
```
$ docker system prune
```

##### Same as above plus unused volumes
```
$ docker system prune --volumes
```
##### Copy file form container to host
```
$ docker cp <CONTAINER ID/NAME>:<CONTAINER'S FILE PATH> <HOST'S FILE PATH>
```
##### Copy file form host to container
```
$ docker cp <HOST'S FILE PATH> <CONTAINER ID/NAME>:<CONTAINER'S FILE PATH>
```
##### Copy directory form container to host
```
$ docker cp <CONTAINER ID/NAME>:<CONTAINER'S DIRECTORY PATH>/. <HOST'S DIRECTORY PATH>/.
```
##### Copy directories form host to container
```
$ docker cp <HOST'S DIRECTORY PATH>/. <CONTAINER ID/NAME>:<CONTAINER'S DIRECTORY PATH>/.
```
## Create networks
```
$ docker network create --driver bridge my_isolated_nw
```
> You will be able to reach the containers from each other using DNS entries corresponding to the container name.

## Playing with netcat
```
$ docker network create --driver bridge mynw

$ docker run -it --rm --name myub1 --network mynw ubuntu:bionic

$ docker run -it --rm --name myub2 --network mynw ubuntu:bionic
```

##### Then in each container's console
```
# apt-get update

# apt-get install netcat
```

##### In myub1 (server)
```
# nc -l -p 55555
```

##### In myub2 (client)
```
# nc myub1 55555
```

## Dockerfile
```
FROM ubuntu:bionic

RUN apt-get -y update
RUN apt-get -y install nodejs
RUN apt-get -y install npm
RUN apt-get -y install netcat

WORKDIR /usr/src/app

COPY . .

RUN npm install

CMD bash wait_for_mongo.sh
CMD node bin/www
```

## Docker Compose
> Must be run in the same directory where the docker-compose.yml file is located
##### Build Project
```
# docker-compose build
```
##### Run Project
```
# docker-compose up
```
> "Ctrl + C" to stop
##### Run Project in detach mode
```
# docker-compose up -d
```
##### Stop detached Project
```
# docker-compose stop
```
#### Example with out nginx
##### docker-compose.yml
```
version: '3.0'
services:
  app:
    image: mean-docker-compose
    build: .
    networks:
      - my-net
    ports:
      - "3000:3000"
    depends_on:
      - 'mongo'
    environment:
          - MONGO=mongodb://mongo
          - MONGO_DB=/mymongo
    restart: on-failure
  mongo:
    image: mongo:latest
    networks:
      - my-net
    expose:
          - "27017"
networks:
  my-net:
    driver: bridge
```

#### Example with nginx
##### docker-compose.yml
```
version: '3.0'
services:
  app:
    image: mean-docker-compose
    build: .
    networks:
      - my-net
    expose:
      - "3000"
    depends_on:
      - 'mongo'
    environment:
      - MONGO=mongodb://mongo
      - MONGO_DB=/mymongo
    restart: on-failure
  mongo:
    image: mongo:latest
    networks:
      - my-net
    expose:
      - "27017"
  nginx:
    image: nginx:latest
    volumes:
      - ./nginx/:/etc/nginx/conf.d/
    networks:
      - my-net
    links:
      - app
    ports:
      - 80:80
      - 443:443
    restart: on-failure
networks:
  my-net:
    driver: bridge
```
> The app container must expose the port, do not bind it to the host.
#### Setting up nginx
##### nginx.conf
```
server {
  listen 0.0.0.0:80;
  server_name  localhost;

  location / {
    proxy_pass http://app:3000;
  }
}
server {
  listen 0.0.0.0:443;
  server_name  localhost;

  location / {
    proxy_pass http://app:3000;
  }
}
```
> For other implementations, the location must be change from app:3000 to your own <CONTAINER_NAME>:<CONTAINER_PORT>