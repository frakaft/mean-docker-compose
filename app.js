var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var tasksRouter = require(path.resolve('routes/tasks'));

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.resolve('public')));

/* API's URIs always first */
app.use('/api/v1/tasks/', tasksRouter);

/* Angular single page application */
app.use(function (req, res) {
    res.sendFile(path.resolve('public/index.html'));
});

module.exports = app;
